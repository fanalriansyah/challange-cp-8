import React, { useState } from "react";
import "./App.css";
import PlayerFrom from "./components/PlayerForm";
import PlayerList from "./components/PlayerList";

function App() {
  const [players, setPlayers] = useState([]);

  function addPlayer(player) {
    setPlayers([player, ...players]);
  }

  return (
    <div class="container mt-5">
      <div class="row">
        <div className="col"></div>
        <div className="col-sm-6">
          <p class="fs-2">Player Form</p>
          <PlayerFrom addPlayer={addPlayer} />
        </div>
        <div className="col"></div>
      </div>
      <div class="row">
        <div className="col"></div>
        <div className="col-sm-6">
          <PlayerList players={players} />
        </div>
        <div className="col"></div>
      </div>
    </div>
  );
}
export default App;
