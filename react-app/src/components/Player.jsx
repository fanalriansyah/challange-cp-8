import React from "react";

function Player({ player }) {
  return (
    <tbody>
      <tr>
        <th scope="row">{player.id}</th>
        <td>{player.username}</td>
        <td>{player.email}</td>
        <td>{player.experience}</td>
        <td>{player.level}</td>
      </tr>
    </tbody>
  );
}

export default Player;
