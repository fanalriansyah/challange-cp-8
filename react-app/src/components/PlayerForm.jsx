import React, { useState } from "react";
import { v4 as uuid } from "uuid";
const id = uuid();

function PlayerFrom({ addPlayer }) {
  const [player, setPlayer] = useState({
    id: "",
    username: "",
    email: "",
    experience: "",
    level: "",
  });

  function handleInputChance(e) {
    setPlayer({ ...player, [e.target.name]: e.target.value });
  }

  function handleSubmit(e) {
    e.preventDefault();
    if (!player.email.trim() || !player.username.trim()) {
      return alert("Username or Email Cannot Empty!");
    }

    addPlayer({ ...player, id: id });
    setPlayer({
      ...player,
      username: "",
      email: "",
      experience: "",
      level: "",
    });
  }

  return (
    <form onSubmit={handleSubmit}>
      <div class="mb-3">
        <label for="username" class="form-label">
          Username
        </label>
        <input
          type="text"
          class="form-control"
          name="username"
          value={player.username}
          onChange={handleInputChance}
          placeholder="Your username..."
        />
      </div>
      <div class="mb-3">
        <label for="email" class="form-label">
          Email address
        </label>
        <input
          type="email"
          class="form-control"
          name="email"
          value={player.email}
          onChange={handleInputChance}
          placeholder="Your email..."
        />
      </div>
      <div class="mb-3">
        <label for="experience" class="form-label">
          Experience
        </label>
        <input
          type="number"
          class="form-control"
          name="experience"
          value={player.experience}
          onChange={handleInputChance}
          placeholder="Your experience..."
        />
      </div>
      <div class="mb-3">
        <label for="lastName" class="form-label">
          Level
        </label>
        <input
          type="number"
          class="form-control"
          name="level"
          value={player.level}
          onChange={handleInputChance}
          placeholder="Your level..."
        />
      </div>
      <button type="submit" class="btn btn-primary">
        Submit
      </button>
    </form>
  );
}

export default PlayerFrom;
