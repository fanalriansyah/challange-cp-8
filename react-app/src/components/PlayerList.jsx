import React from "react";
import Player from "./Player";

function PlayerList({ players }) {
  return (
    <div className="mt-5">
      <p class="fs-2">Player List</p>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">USERNAME</th>
            <th scope="col">EMAIL</th>
            <th scope="col">EXP</th>
            <th scope="col">LEVEL</th>
          </tr>
        </thead>
        {players.map((player) => (
          <Player key={player.id} player={player} />
        ))}
      </table>
    </div>
  );
}

export default PlayerList;
